<?php

$plugin = array(
  'title' => 'Social: Links',
  'category' => 'GK Social',
  'single' => TRUE,
);

function gk_social_gk_social_links_content_type_render($subtype, $conf, $args, $context) {
  if ($link_urls = variable_get('gk_social_platform_links')) {
    $content = array();
    $site_name = variable_get('site_name');
    $link_options = gk_social_platform_link_options();

    foreach ($link_urls as $key => $url) {
      if (!empty($url) && isset($link_options[$key])) {
        $content[$key] = array(
          '#theme' => 'link',
          '#text' => t('<span>View @site_name\'s @platform profile</span>', array(
            '@site_name' => $site_name,
            '@platform' => $link_options[$key],
          )),
          '#path' => $url,
          '#options' => array(
            'html' => TRUE,
            'attributes' => array(
              'class' => array($key),
              'target' => '_blank',
            ),
          ),
        );
      }
    }

    return (object) array(
      'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
      'content' => $content,
    );
  }
}

function gk_social_gk_social_links_content_type_edit_form($form, &$form_state) {
  return $form;
}
